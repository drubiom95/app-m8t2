package com.tph.m8t2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.m8t2.totp.R;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.CodeVerifier;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.DefaultCodeVerifier;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.QrGenerationException;
import dev.samstevens.totp.qr.QrData;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String ANDROID_PACKAGE = "application/vnd.android.package-archive";

    private Context context;

    private Button StockTakeButton;
    private Button EppButton;
    private String version;
    private Button Button2;
    private Button CompruebaButton;
    private ImageView ImageView;
    private EditText EditText;

    private String newApk = null;

    private String secret;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        version = getResources().getString(R.string.num_version);
        context = this;
        this.Button2 = (Button) findViewById(R.id.button2);
        this.CompruebaButton = (Button) findViewById(R.id.compruebaButton);
        this.ImageView = (ImageView) findViewById(R.id.imageView2);
        this.EditText   = (EditText)findViewById(R.id.editTextNumber);
        try {
            generaNuevoQR(false);
            generaNuevoQR(false);
        } catch (QrGenerationException | WriterException e) {
            e.printStackTrace();
        }
        this.Button2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    generaNuevoQR(true);
                } catch (QrGenerationException | WriterException e) {
                    e.printStackTrace();
                }
                //Intent intent = new Intent(context, EppLogin.class);
                //startActivity(intent);
            }
        });
        this.CompruebaButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TimeProvider timeProvider = new SystemTimeProvider();
                CodeGenerator codeGenerator = new DefaultCodeGenerator();
                CodeVerifier verifier = new DefaultCodeVerifier(codeGenerator, timeProvider);

                // secret = the shared secret for the user
                // code = the code submitted by the user
                boolean successful = verifier.isValidCode(secret, EditText.getText().toString());
                if(successful){
                    Intent intent = new Intent(MainActivity.this, CodeOk.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MainActivity.this, ErrorCode.class);
                    startActivity(intent);
                }
                //Intent intent = new Intent(context, EppLogin.class);
                //startActivity(intent);
            }
        });

        //new CheckUpdateTask().execute();
    }

    private void generaNuevoQR(boolean nuevo) throws QrGenerationException, WriterException {
        SharedPreferences myPreferences
                = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SecretGenerator secretGenerator = new DefaultSecretGenerator();
        String name = myPreferences.getString("SECRET", "unknown");
        if(name.equals("unknown") || nuevo){
            SharedPreferences.Editor myEditor = myPreferences.edit();
            secret = secretGenerator.generate();
            myEditor.putString("SECRET", secret);
            myEditor.commit();
        }else{
            secret = name;
        }
        QrData data = new QrData.Builder()
                .label("davidRubio")
                .secret(secret)
                .issuer("M8T2-TOTP")
                .algorithm(HashingAlgorithm.SHA1) // More on this below
                .digits(6)
                .period(30)
                .build();
        QRGEncoder qrgEncoder = new QRGEncoder(data.getUri(), null, QRGContents.Type.TEXT, ImageView.getWidth());
        // Getting QR-Code as Bitmap
        Bitmap bitmap = qrgEncoder.encodeAsBitmap();
        // Setting Bitmap to ImageView
        ImageView.setImageBitmap(bitmap);
        /*QrGenerator generator = new ZxingPngQrGenerator();
        byte[] imageData = generator.generate(data);
        Bitmap bmp = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        imgViewer.setImageBitmap(Bitmap.createScaledBitmap(bmp, imgViewer.getWidth(), imgViewer.getHeight(), false));*/
    }

}
