package com.tph.m8t2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.m8t2.totp.R;

import java.io.*;

public class ErrorCode extends Activity {

    private Context context;
    private String fileName;
    private String fileSrc;
    private boolean copied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_error_code);

        context = this;
    }

    public void back(View view) {

        finish();
    }



    public void onTaskCompleted() {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Toast.makeText(context, "Copiado en /Android/stock-take-data/", Toast.LENGTH_LONG).show();
    }

    private class CopyFileTask extends AsyncTask<Void, Void, Void> {

        private ErrorCode listener;

        public CopyFileTask(ErrorCode listener) {
            this.listener = listener;
        }

        protected Void doInBackground(Void... arg0) {
            try {
                File src = new File(fileSrc);
                BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(src)));
                String line;
                StringBuilder builder = new StringBuilder();
                while ((line = input.readLine()) != null) {
                    builder.append(line).append("\r\n");
                }
                File dst = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/stock-take-data/");
                dst.mkdir();

                for (File file : dst.listFiles()) {
                    if (!file.isDirectory()) {
                        String absolutePathToFile = file.getAbsolutePath();
                        file.delete();
                        Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        mediaScannerIntent.setData(Uri.fromFile(new File(absolutePathToFile)));
                        sendBroadcast(mediaScannerIntent);
                    }
                }

                dst.setExecutable(true);
                dst.setReadable(true);
                dst.setWritable(true);

                String fileDst = dst.getAbsolutePath() + "/" + fileName;
                FileOutputStream outputStream = new FileOutputStream(new File(fileDst));
                outputStream.write(builder.toString().getBytes());
                outputStream.close();
                Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaScannerIntent.setData(Uri.fromFile(new File(fileDst)));
                sendBroadcast(mediaScannerIntent);
                copied = true;
            } catch (Exception e) {
                copied = false;
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            listener.onTaskCompleted();
        }
    }

}
